﻿using System;
using System.Collections.Generic;
using System.Text;

using BEAR.Models;

namespace BEAR.ViewModels
{
    public class MenuViewModel
    {
        public List<MasterMenuItem> mainMenuList { get; set; }

        public MenuViewModel()
        {
            mainMenuList = new List<MasterMenuItem>();
            mainMenuList.Add(new MasterMenuItem { Id = MasterMenuItemType.Connect, Title = "Connect" });
            mainMenuList.Add(new MasterMenuItem { Id = MasterMenuItemType.Configure, Title = "Configure" });
            mainMenuList.Add(new MasterMenuItem { Id = MasterMenuItemType.Settings, Title = "Settings" });
        }
    }
}
