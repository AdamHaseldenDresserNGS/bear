﻿using BEAR.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using BEAR.ViewModels;

namespace BEAR.Views
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MasterMenuPage : ContentPage
    {
        MainPage RootPage { get => Application.Current.MainPage as MainPage; }
        public MasterMenuPage()
        {
            MenuViewModel menuViewModel = new MenuViewModel();

            InitializeComponent();

            ListViewMenu.ItemsSource = menuViewModel.mainMenuList;
            ListViewMenu.SelectedItem = menuViewModel.mainMenuList[0];
            ListViewMenu.ItemSelected += async (sender, e) =>
            {
                if (e.SelectedItem == null)
                    return;

                var id = (int)((MasterMenuItem)e.SelectedItem).Id;
                await RootPage.NavigateFromMenu(id);
            };
        }
    }
}