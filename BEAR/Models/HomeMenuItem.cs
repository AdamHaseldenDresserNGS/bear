﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BEAR.Models
{
    public enum MasterMenuItemType
    {
        Connect,
        Configure,
        Settings
    }

    public class MasterMenuItem
    {
        public MasterMenuItemType Id { get; set; }

        public string Title { get; set; }
    }
}
